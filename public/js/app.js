/*
 MacysHTML5Player 

 Self contained HTML5 Shoppable Videoplayer class

 VideoPlayer
 // wrapper for HTML5 video built in methods / media events

 CustomVideoPlayer
 // VideoPlayer + all purpose custom controller GUI

 ShopablePlayer

 ++ cuepoint and product timeline display
 */

    var MacysHTML5Player = (function (container, params) {
        console.log("creating MacysHTML5Player", container, params);
        var _self = this;
        this.isPlayerAdded = false;
        this.isPaused = true;
        if (typeof this.container !== "undefined") {
            $(this.container).empty();
            delete this.container;
        }

        this.container = document.getElementById(container);
        // TODO remove for production: for testing development only
        this.isTablet = /Android|webOS|iPad/i.test(navigator.userAgent);
        function Timer(callback, delay) {
            var timerId, start, remaining = delay;
            var self = this;
            this.pause = function () {
                if (self.paused)
                    return;
                self.paused = true;
                window.clearTimeout(timerId);
                remaining -= new Date() - start;
                console.log("TIMER PAUSED", timerId, remaining);
            };

            this.resume = function () {
                self.paused = false;
                start = new Date();
                timerId = window.setTimeout(callback, remaining);
                console.log("TIMER RESUME", timerId, remaining);
            };

            this.resume();
        }

        var BasicPlayer = Class.extend({
            init: function (container, params) {
                if (typeof this.params == "undefined") {
                    this.params = params;
                }
                this.events = [];
                //TODO currently only unique player support
                this.container = document.getElementsByClassName("mp-container")[0];

                this.container.setAttribute("style", "position:relative;width:" + this.params.width + "px;" + "height:" + this.params.height + "px;");
                _self.container.setAttribute("width", this.params.width + "px");
                _self.container.setAttribute("height", this.params.height + "px");
                this.video = document.createElement("video");

                if (this.params.controls)
                    this.video.setAttribute("controls");

                this.container.appendChild(this.video);
                var self = this;
                this.video.addEventListener("loadstart", function (e) {
                    console.log("MacysHTML5VideoPlayer:: LOADED", e);
                    self.dispatchEvent(new Event("onReady"));
                });
                this.video.addEventListener("error", function (e) {
                    console.log("MacysHTML5VideoPlayer:: ERROR", $(e));
                });
            },
            setData: function (data) {
                if (this.data)
                    delete this.data;
                console.log("BasicPlayer::setData", data, data.videoURL);
                var d = {};
                var cp = ($.parseJSON(data.cuePoints));
                var pool = ($.parseJSON(data.poolInfo));
                d.cuePoints = cp.cuePoints_arr;
                d.productPool = pool.products_arr;
                this.data = d;

                if (typeof data.poster != "undefined")
                    this.video.poster = data.poster;

                this.video.src = data.videoURL;
            },
            addEventListener: function (e, fn) {
                console.log(this.events.indexOf(e), "this.events.indexOf(e)")
                //prevent multiple events
                var ref = {event: e, func: fn};
                if (!this.events.indexOf(ref)) {
                    return;
                }
                this.events[this.events.length] = ref;
                this.container.addEventListener(e, fn);
            },
            dispatchEvent: function (e) {
                this.container.dispatchEvent(e);
            },
            currentTime: function () {
                return this.video.currentTime;
            },
            setTime: function (time) {
                this.time = time;
                this.video.currentTime = time;
                // return this.video.play();
            },
            unload: function () {
                for (var i in this.events) {
                    this.container.removeEventListener(this.events[i].event, this.events[i].func);
                    console.log("removeEventListener", this.events[i].event, this.events[i].func);
                }
            },
            progress: function () {
                return Math.round((100 * (this.video.currentTime / this.video.duration)));
            },
            play: function () {
                return this.video.play();
            },
            playToggle: function () {
                (!this.video.paused) ? this.video.pause() : this.video.play();
                return this.video.paused;
            },
            pause: function () {
                console.log("SVP::pause");
                if (!this.video.paused) {
                    return this.video.pause();
                }
            },
            isPaused: function () {
                return this.video.paused;
            }
        });
        // --------------------------------------------------------------------------------------------- //
        //shoppable starts
        var CustomPlayer = BasicPlayer.extend({
            init: function (container, params) {

                if (typeof this.params == "undefined")
                    this.params = params;

                this._super(container, this.params);

                var self = this;

                this.$videoControls = $(".mp-video-controls", this.container);
                this.$loader = $(".mp-loader", this.container);
                this.$playButton = $(".mp-play-button", this.container);
                this.$seekBar = $(".mp-seek-bar", this.container);
                this.$seekTarget = $(".mp-seek-target", this.container);
                this.$scrubBtn = $(".mp-progress:after", this.container);
                this.$bufferBar = $(".mp-buffer-bar", this.container);
                this.$seekContainer = $(".mp-seek-container", this.container);
                this.$nowTime = $(".mp-now-time", this.container);
                this.$totalTime = $(".mp-total-time", this.container);
                this.$progressBar = $(".mp-progress", this.$videoControls)

                this.$startBtn = $('.mp-start-btn', this.container);

                this.$startBtn.click(function (e) {
                    e.preventDefault();
                    self.play();
                    self.dispatchEvent(new Event("cuePointAllOut"));
                });

                this.$playButton.click(function (e) {
                    e.preventDefault();
                    console.log("CustomVideoPlayer::click play/pause");
                    self.playToggle();
                });

                var buffered = 0;
                this.video.addEventListener("timeupdate", function () {
                    self.updateTimeDisplay();

                    if (!this.seeking) {

                        self.$progressBar.css({
                            'width': self.progress() + '%',
                            '-webkit-transition': 'width .2s ease-out'
                        });

                    }
                    try {
                        buffered = 100 * (this.buffered.end(0) / this.duration);
                    } catch (err) {
                        console.log("Error Buffering: " + err, this.buffered, this.duration);
                    }


                    self.$bufferBar.css({
                        'width': buffered + '%',
                        '-webkit-transition': 'width .2s ease-out'
                    });


                });

                this.video.addEventListener("durationchange", function () {
                    console.log("VIDEO DURATION CHANGED");
                    self.videoDuration = self.videoBuffer = self.video.duration;
                });

                this.initSeekBar();

                this.video.addEventListener("loadeddata", function () {
                    self.$loader.hide(function () {
                        if (self.$videoControls.css('display') == "none") {
                            self.$videoControls.fadeIn();
                        }
                    });
                });
                this.video.addEventListener("waiting", function () {
                    console.log("VIDEO WAITING");
                    self.$loader.show();
                });
                this.video.addEventListener("play", function () {
                    self.$startBtn.fadeOut(function () {
                        if (self.$videoControls.css('display') == "none")
                            self.$videoControls.fadeIn();

                    });
                    self.$playButton.show();

                    self.$playButton.addClass("mp-paused");

                    console.log("VIDEO PLAYING", self.paused);

                });

                this.video.addEventListener("pause", function () {

                    self.$playButton.removeClass("mp-paused");

                    console.log("VIDEO PAUSED", this.paused);
                });

                this.video.addEventListener("ended", function () {
                    console.log("VIDEO COMPLETED");
                    self.$videoControls.hide();
                    self.$startBtn.show();
                });
            },
            updateTimeDisplay: function () {
                this.$nowTime.html(this.formatTime(this.video.currentTime));
                this.$totalTime.html(this.formatTime(this.videoDuration));
            },
            initSeekBar: function () {
                var self = this;
                this.seeking = false;
                // TODO remove for production: for testing development only
                if (this.isTablet) {

                    function seekTouch(e, type) {
                        console.log(e.name, "seeking");
                        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

                        self.seekTime(touch.pageX);
                    }

                    // aEvents = ["touchstart","touchmove","touchstop", "touchend", "touchleave"];
                    this.$seekTarget.bind("touchstart", function (e) {
                        e.preventDefault();
                        self.seeking = true;
                    });
                    this.$seekTarget.bind("touchmove", function (e) {
                        e.preventDefault();
                        if (self.seeking) {
                            seekTouch(e);
                        }
                    });
                    this.$seekTarget.bind("touchstop",function (e) {
                        e.preventDefault();
                        self.seeking = false;
                        seekTouch(e);
                    }).bind("touchend",function (e) {
                            e.preventDefault();
                            self.seeking = false;
                            seekTouch(e);
                        }).bind("touchleave", function (e) {
                            e.preventDefault();
                            self.seeking = false;
                        });
                } else {
                    console.log("initSeekBar :: NOT IPAD");

                    this.$seekTarget.mousedown(function (e) {
                        e.preventDefault();
                        self.seeking = true;
                        console.log("mousedown", "seeking", e.pageX);
                    }).mousemove(function (e) {
                            e.preventDefault();
                            if (self.seeking) {
                                self.seekTime(e.pageX);
                                // console.log("mousemove", "seeking", e.pageX);
                            }
                        }).mouseup(function (e) {
                            e.preventDefault();
                            self.seekTime(e);
                            console.log("mouseup", "seeking");
                            self.seeking = false;

                            if (!self.seeking) {
                                self.seekTime(e.pageX);
                                console.log("mouseup", "seeking", e.pageX);
                            }
                        }).mouseleave(function (e) {
                            console.log("mouseleave", "seeking");
                            self.seeking = false;
                            //	updateTime(e);
                        });
                }
            },
            seekTime: function (ex) {
                var pageX = ex - this.$seekBar.offset().left;
                // console.log("seekTime() pageX: ", pageX, $(".seekBar", this.container).width(), $(".seekBar", this.container).offset().left);
                if (pageX >= this.$seekBar.width()) {
                    console.log("STOP SEEK");
                    return;
                }
                var perc = (pageX / this.$seekBar.width());
                // TODO consolidate with timeupdate
                this.$progressBar.css("width", (perc * 100) + "%", '-webkit-transition : width .2s linear');
                this.setTime(this.videoDuration * perc);
            },
            formatTime: function (msTime) {
                var seconds = Math.round(msTime % 60);
                var minutes = Math.round(msTime / 60);
                if (seconds < 10)
                    seconds = "0" + seconds;
                return minutes + ":" + seconds;
            }
        });

        var CuePoint = Class.extend({
            init: function (params) {
                // cuepoint marker
                this.id = params.id;
                this.timecode = params.timecode;
                this.productTime = 4000;
                this.container = document.createElement("div");
                this.container.className = "cp-container";
                this.container.setAttribute("data-id", params.id);
                this.container.setAttribute("style", "left:" + params.xPos + "px");

                this.marker = document.createElement("div");
                this.marker.className = "cp-marker";
                this.marker.setAttribute("data-id", params.id);
                this.marker.setAttribute("data-timecode", params.timecode);

                this.container.appendChild(this.marker);

                if (params.toLeft) {
                    this.toLeft = true;
                    $(this.container).addClass("cp-product-left");
                } else {
                    this.toLeft = false;
                }
                this.productContainer = this.setProducts(params.products);
                this.productContainer.addEventListener('webkitTransitionEnd', function (event) {
                    event.preventDefault();
                    console.log("DONE ANIMATING");
                    $(this).css("pointer-events", "auto");
                }, false);
                this.container.appendChild(this.productContainer);
                this.collapse();

            },
            setProducts: function (products) {
                var self = this;
                var pdHTML = '';

                this.productContainer = document.createElement("div");
                this.productContainer.setAttribute("class", "cp-product-container");

                this.productContainer.setAttribute("id", "cp-product-container" + this.id);


                // important to declare target first
                this.target = document.createElement("div");
                this.target.className = "cp-target";
                this.target.setAttribute("data-id", this.id);
                this.target.setAttribute("id", "cp-target" + this.id);
                this.target.onclick = function () {
                    console.log(this, "click");
                    self.currentCue = this.getAttribute("data-id");
                    self.expand();
                };

                var pd, pdImg;

                for (var i in products) {
                    if (typeof products[i].id != "undefined") {

                        if (typeof products[i].image !== "undefined" && $.trim(products[i].image).length) {
                            pdImg = 'style="background-image:url(' + (products[i].image) + ');'
                        }
                        pdHTML = '<div class="cp-product-wrapper">';
                        pdHTML += '<div class="cp-img"  ' + pdImg + '">';
                        pdHTML += '</div><div class="cp-shop-btn"></div></div>';

                        pd = document.createElement("div");
                        pd.setAttribute("class", "cp-product");
                        pd.setAttribute("data-product-id", products[i].id);
                        pd.innerHTML = (pdHTML);
                        this.productContainer.appendChild(pd);
                    }
                }

                $(".cp-product", $(self.productContainer)).click(function (e) {
                    try {
                        onShopNowClick($(this).attr("data-product-id")); // to skava
                    } catch (err) {
                        console.log("onShowNowClick: ", $(this).attr("data-product-id"));
                    }
                });


                this.productContainer.insertBefore(this.target, this.productContainer.firstChild)

                return this.productContainer;
            },
            hide: function () {
                this.marker.setAttribute("style", "");
                $(this.productContainer).fadeOut();
                $(this.marker).removeClass("cp-active");

                if (typeof this.timer != "undefined") {
                    this.timer.pause();
                    delete this.timer;
                }
            },
            show: function () {
                var self = this;
                this.marker.setAttribute("style", "pointer-events:none;");

                $(this.productContainer).fadeIn('fast');

                $(this.productContainer).addClass("cp-transition-in");

                // $(this.productContainer).delay(450, function(){this.removeClass("cp-transition-in");})
                $(this.marker).addClass("cp-active");
                if (typeof this.timer == "undefined") {
                    this.startTimer();
                }
            },
            expand: function (id) {
                if (this.expanded) {
                    console.log("already expanded", this.expanded);
                    return;
                }
                this.pause();
                this.expanded = true;
                var d = 130;
                var orientation = '';
                if (this.toLeft) {
                    orientation = "-";
                }
                this.target.setAttribute("style", "pointer-events:none;background-position: 14% 100%;");

                var count = $(".cp-product", $(this.productContainer)).length;

                $(this.productContainer).addClass("cp-expanded");
                $(".cp-product", $(this.productContainer)).each(function (index, value) {
                    $(value).attr("style", "left:" + orientation + index * d + "px;top:0px;z-index:" + Math.abs(index - count));
                });

                $(this.container).css("width", (count * d) + "px");
            },
            collapse: function () {
                var d = 15;
                var dg = 2.75;
                var orientation = '';
                if (this.toLeft) {
                    orientation = "-"
                }

                this.resume();
                this.container.setAttribute("width", $(this.target).width());
                this.expanded = false;

                var count = $(".cp-product", $(this.productContainer)).length;
                this.target.setAttribute("style", "height:" + ((count * d) + 238) + "px; width:" + ((count * d) + 130) + "px;top:-" + ((count * d)) + "px;z-index:" + count + 1);
                $(this.productContainer).removeClass("cp-expanded");
                $(".cp-product", $(this.productContainer)).each(function (index, value) {

                    $(value).attr("style", "left:" + orientation + index * d + "px; top:-" + (index * d) + "px;z-index:" + Math.abs(index - count) + "; -webkit-transform:rotate(" + orientation + +(index * dg) + "deg)");
                });
            },
            startTimer: function () {
                var self = this;
                if (typeof this.timer != "undefined")
                    delete this.timer;
                console.log("timer start")
                this.timer = new Timer(function () {
                    console.log("timer end");
                    self.hide();
                }, self.productTime);
            },
            pause: function () {
                if (typeof this.timer != "undefined") {
                    this.timer.pause();
                }
            },
            resume: function () {
                if (typeof this.timer != "undefined" && !this.expanded) {
                    this.timer.resume();
                }
            }
        });

        var ShopablePlayer = CustomPlayer.extend({
            init: function (container, params) {
                this.params = params;
                this.timeAfterOffset = 1.5;
                this.$bgTarget = $(".mp-bg-btn", this.container);
                this._super(container, params);

                this.setup();

            },

            setData: function (data) {
                this._super(data);
                this.initialized = false;
            },
            setup: function () {

                var self = this;
                // this.initialized = false;
                this.paused = true;
                this.video.addEventListener("loadeddata", function () {
                    console.log("ShopablePlayer::loadeddata !!")
                    // TODO loadeddata event fires twice

                    self.setCuePoints(self.data.cuePoints);
                    self.setTime(0);
                });

                this.addEventListener("cuePointAllOut", function () {
                    if (typeof self.cuePoints != "undefined") {
                        self.cuePoints.forEach(function (e) {
                            e.hide();
                            e.collapse();
                        });
                    }
                });
                this.addEventListener("cuePointIn", function (e) {
                    e.preventDefault();
                    console.log("cuePointIn");
                    if (typeof self.currentCue != "undefined" && self.cuePoints[self.currentCue].expanded && !self.video.seeking) {
                        return;
                    }
                    for (var i = 0; i < self.cuePoints.length; i++) {
                        if (e.id != self.cuePoints[i].id) {
                            self.cuePoints[i].hide();
                            self.cuePoints[i].collapse();
                        } else {
                            self.currentCue = e.id;
                            self.cuePoints[i].show();
                        }
                    }
                });

                this.video.addEventListener("play", function () {
                    self.$bgTarget.show();
                    if (self.paused) {
                        self.paused = false;
                        if (typeof self.currentCue != "undefined") {

                            self.cuePoints[self.currentCue].resume();
                        }
                    }
                    console.log("VIDEO PLAYING", self.paused);

                });
                this.video.addEventListener("pause", function () {
                    if (!self.paused) {
                        self.paused = true;
                        // console.log(self.currentCue, 'timer');
                        if (typeof self.currentCue != "undefined") {

                            self.cuePoints[self.currentCue].pause();
                        }
                    }
                    // console.log("VIDEO PAUSED", this.paused);
                });

                this.video.addEventListener("ended", function () {
                    console.log("VIDEO COMPLETED");

                    self.dispatchEvent(new Event("cuePointAllOut"));

                    self.$bgTarget.hide();
                });

                this.$bgTarget.click(function (e) {
                    e.preventDefault();
                    self.dispatchEvent(new Event("cuePointAllOut"));
                });



            },
            addCuePoint: function (time, id) {
                var self = this;
                this.time = time;

                var fn = function () {
                    //console.log("cuepoint added", time, id)
                    if (self.video.currentTime >= time && self.video.currentTime <= time + self.timeAfterOffset) {
                        var e = new Event(("cuePointIn"));
                        e.id = id;
                        self.container.dispatchEvent(e);
                    }
                }
                this.video.addEventListener("timeupdate", fn, false);
                return fn;
            },
            setCuePoints: function (a) {
                console.log("setCuePoints()", $(".cp-container"));

                // reset cuepoints
                $(".cp-container").remove();
                this.cuePoints = [];

                this.expanded = false;
                var self = this;
                var seekWidth = this.$seekBar.width();
                var o = {};
                // TODO fix offset & use handlebars
                var seekOffset = .025;

                if (typeof this.cueEvents != "undefined") {
                    for (var i in this.cueEvents) {
                        this.video.removeEventListener("timeupdate", this.cueEvents[i]);
                    }
                }
                // TODO consolidate cueEvents into cuePoints
                this.cueEvents = [];

                for (var i in a) {
                    o = {};
                    o.timecode = a[i].timecode;
                    o.id = i;

                    o.xPos = (Math.round((seekWidth * o.timecode / this.videoDuration)) + (seekWidth * seekOffset));
                    console.log(o.xPos, "o.xPos");
                    // outside of class
                    if ((a[i].timecode / this.videoDuration) >= .55) {
                        o.toLeft = true;
                    }
                    try {
                        o.products = this.getProducts(a[i].products);
                        this.cuePoints[i] = new CuePoint(o);
                        this.cueEvents[this.cueEvents.length] = this.addCuePoint(o.timecode, i);
                        this.$seekContainer[0].appendChild(this.cuePoints[i].container);
                    } catch (e) {
                        console.log("setCuePoints() Error: ", e, i, o, a);
                    }


                }

            },
            getProducts: function (ap) {
                ap = ap.split(",");
                var pp = this.data.productPool;
                var products = []
                for (i = 0; i < ap.length; i++) {
                    for (j = 0; j < pp.length; j++) {
                        if (ap[i] == pp[j].id) {
                            products[products.length] = pp[j];
                            break;
                        }

                    }
                }

                return products;
            }
        });
        // TODO add type switch
        // embed
        this.videoType = params.type;
        this.videoHTML = "<div id=\"mp-container\"></div>";
        //  console.log(arguments[1], "args");
        if (this.videoType != "basic") {
            this.videoHTML = '<div class="mp-container">' +
                '<div class="mp-bg-btn"></div>'
                + '<div class="mp-start-btn"></div>'
                + '<div class="mp-loader"></div>'
                + '<div class="mp-video-controls">'
                + '<div class="mp-wrapper">'
                + '<div class="mp-play-button mp-interface"></div>'
                + '<div class="mp-seek-container mp-interface">'
                + '<div class="mp-seek-wrapper">'
                + '<div class="mp-seek-bar mp-slider">'
                + '<div class="mp-buffer-bar mp-slider"></div>'
                + '<div class="mp-progress mp-slider"></div></div>' +

                '</div>'
                + '<div class="mp-seek-target"></div>'
                + '</div>'
                + '<div class="mp-time-display mp-interface">'
                + '<div class="mp-now-time"></div>'
                + '<div class="mp-total-time"></div>'
                + '</div>'
                + '</div>'
                + '</div></div>';
        }

        this.container.innerHTML = (this.videoHTML);

        if (typeof this.videoPlayer !== "undefined")
            delete this.videoPlayer;

        switch (this.videoType) {
            case "shopable":
                this.videoPlayer = new ShopablePlayer(arguments[0], arguments[1]);
                break;
            case "custom":
                this.videoPlayer = new CustomPlayer(arguments[0], arguments[1]);
                break;
            case "basic":
                this.videoPlayer = new BasicPlayer(arguments[0], arguments[1]);
                break;
            default:
                this.videoPlayer = new ShopablePlayer(arguments[0], arguments[1]);
                break;
        }

        return this.videoPlayer;

    });
